/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7_ferreira;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MARCOS.D.F
 */
public class PackFuncionesTest {
    
    public PackFuncionesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of factorial method, of class PackFunciones.
     */
    @Test
    public void testFactorial() {
        System.out.println("factorial");
        int n = 5;
        //PackFunciones instance = new PackFunciones();
        int expResult = 120;
        int result = PackFunciones.factorial(n);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of fibonacci method, of class PackFunciones.
     */
    @Test
    public void testFibonacci() {
        System.out.println("fibonacci");
        int n = 1;
        PackFunciones instance = new PackFunciones();
        int expResult = 1;
        int result = instance.fibonacci(n);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }

    /**
     * Test of cuadrante method, of class PackFunciones.
     */
    @Test
    public void testCuadrante() {
        System.out.println("cuadrante");
        int x = -3;
        int y = 8;
        PackFunciones instance = new PackFunciones();
        String expResult = "El punto (-3,8) esta en el primer cuadrante";
        String result = instance.cuadrante(x, y);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
}
