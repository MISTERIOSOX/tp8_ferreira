/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7_ferreira;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MARCOS.D.F
 */
public class RaicesTest {
    
    public RaicesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getX1 method, of class Raices.
     */
Raices instance = new Raices();
    @Test
    public void testCalcularRaices() {
         System.out.println("calcularRaices");
        int a = 1;
        int b = -2;
        int c = -24;
        
        Raices result =instance.calcularRaices(a, b, c);
        float x1=7;
        float x2=-3;
        String tip="La Ecuacion No Tiene Soluciones Reales";
             
        float expResult1 = instance.getX1();
        float expResult2 = instance.getX2();
        String frase=instance.getTipo();
        assertEquals(expResult1, x1, x1);
        assertEquals(expResult2, x2, x2);
        assertEquals(tip, frase, frase);
    
    }
    @Test
    public void testCalcularRaices1() {
        System.out.println("calcularRaices");
        int a1 = 2;
        int b1 = 1;
        int c1 = -1;
        
        Raices result =instance.calcularRaices(a1, b1, c1);
        String tip1=instance.getTipo();
        float x2=2;
        float x3=-4;
        String tip="La ecuacion tiene dos raices reales";
             
        float expResult1 = instance.getX1();
        float expResult2 = instance.getX2();
       
       assertEquals(expResult1, x2, x2);
       assertEquals(expResult2, x3, x3);
       assertEquals(tip, tip1, tip1);
        
        
    } 
    @Test
    public void testCalcularRaices2() {
        System.out.println("calcularRaices");
        int a = 2;
        int b = 2;
        int c = 5;
        
        Raices result =instance.calcularRaices(a, b, c);
        String tipo=instance.getTipo();
        float x1=0;
        float x2=0;
        String tip="La Ecuacion No Tiene Soluciones Reales";
             
        float expResult1 = instance.getX1();
        float expResult2 = instance.getX2();
        String frase=instance.getTipo();
        assertEquals(expResult1, x1, x1);
        assertEquals(expResult2, x2, x2);
        assertEquals(tip, frase, frase);
        
        
    } 
    
}
