/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7_ferreira;

/**
 *
 * @author DELL
 */
public class PackFunciones {

    public static int factorial(int n) {
        //calculo del factorial de n mediante recursion
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public int fibonacci(int n) {
        //calculo de fibonnaci mediante una función recursiva
        if (n > 1) {
            return fibonacci(n - 1) + fibonacci(n - 2);
        } else if (n == 1) {
            return 1;
        } else if (n == 0) {
            return 0;
        } else {
            return -1;
        }
    }

    public String cuadrante(int x, int y) {
        if (x < 0 && y > 0) {
            return ("El punto (" + x + "," + y + ") esta en el primer cuadrante");
        } else if (x > 0 && y > 0) {
            return ("El punto (" + x + "," + y + ") esta en el segundo cuadrante");
        } else if (x < 0 && y < 0) {
            return ("El punto (" + x + "," + y + ") esta en el tercer cuadrante");
        } else if (x > 0 && y < 0) {
            return ("El punto (" + x + "," + y + ") esta en el cuarto cuadrante");
        } else {
            return ("El punto (" + x + "," + y + ") esta en el origen o en uno de los ejes.");
        }
    }
}
