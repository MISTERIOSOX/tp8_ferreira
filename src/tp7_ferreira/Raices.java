 
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp7_ferreira;

/**
 *
 * @author DELL
 */
public class Raices {
    private float x1;
    private float x2;
    private String tipo;

    public float getX1() {
        return x1;
    }

    public void setX1(float x1) {
        this.x1 = x1;
    }

    public float getX2() {
        return x2;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Raices calcularRaices(int a, int b, int c) {
        //Raices solucion = new Raices();
        float x1=0, x2=0;
    
        float discriminante = ((b * b) - (4 * (a * c)));

        if (discriminante < 0) {
            this.setTipo("La Ecuacion No Tiene Soluciones Reales");
        }

        if (discriminante == 0) {
            this.setTipo("La ecuacion tiene solo una raiz real");
            x1 = (-b) / (2 * a);
            this.setX1(discriminante);
        }

        if (discriminante > 0) {
            this.setTipo("La ecuacion tiene dos raices reales");
            x1 = (float) (-1*b + Math.sqrt(discriminante) / 2 * a);
            x2 = (float) (-1*b - Math.sqrt(discriminante) / 2 * a);
        }
        this.setX1(x1);
        this.setX2(x2);
        return this;
    }
    
    
}
